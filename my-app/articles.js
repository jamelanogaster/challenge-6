const express = require('express');
const app = express();
const { Article } = require('./models');

app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Get all articles
app.get('/api/v1/articles', (req, res) => {
    Article.findAll()
        .then(articles => {
            res.status(200).json(articles);
        });
});

// Get article by ID
app.get('/api/v1/articles:id', (req, res) => {
    Article.findOne({
            where: { id: req.params.id }
        })
        .then(article => {
            res.status(200).json(article);
        });
});

app.post('/api/v1/articles', (req, res) => {
    if (req.body.approved == null) req.body.approved = false;
    Article.create({
            title: req.body.title,
            body: req.body.body,
            approved: req.body.approved
        })
        .then(article => {
            res.status(200).json(article);
        }).catch(err => {
            res.status(422).json("Can't create article");
        });
});

app.put('/api/v1/articles:id', (req, res) => {
    Article.update({
            title: req.body.title,
            body: req.body.body,
            approved: req.body.approved
        }, {
            where: { id: req.params.id }
        })
        .then(article => {
            res.status(201).json(article);
        }).catch(err => {
            res.status(422).json("Can't update article");
        });
});

// EJS Render
app.get('/articles/create', (req, res) => {
    res.render('articles/create');
});

app.get('/articles', (req, res) => {
    Article.findAll()
        .then(articles => {
            res.render('articles/index', {
                articles
            });
        });
});

app.get('/articles/:id', (req, res) => {
    Article.findOne({
            where: { id: req.params.id }
        })
        .then(article => {
            res.render('articles/show', {
                article
            })
        })
})

// 500 Error Handler
app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

// 404 Error Handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        errors: 'Path not found'
    });
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`App is listening on port ${port}`));
