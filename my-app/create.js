const { Article } = require('./models');

Article.create({
    title: 'Hello World',
    body: 'Lorem ipsum dolor sit amet',
    approved: true
}).then(article => {
    console.log(article);
})